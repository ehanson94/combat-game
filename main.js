let playerOneHealth = document.getElementById('p1Health')
let playerTwoHealth = document.getElementById('p2Health')
let playerMove = document.getElementById('move')

//creating player profile
function Player(name, health) {
    this.name = name
    this.health = health
}

//player 1
const Ninja = new Player('SneakyBoi', 100)
//player 2
const Samurai = new Player('LeeroyJenkins', 100)

let currentPlayer = 'Ninja'

//power attack
Player.prototype.powerAttack = function() {
    if (currentPlayer === 'Ninja') {
        Samurai.health -= 10
        playerTwoHealth.value -= 10
        if (Samurai.health === 0) {
            alert('SneakyBoi has won!!!')
            location.reload()
        }
        currentPlayer = 'Samurai'
        playerMove.innerHTML = 'Player 2 make your move...'
    } else if (currentPlayer === 'Samurai') {
        Ninja.health -= 10
        playerOneHealth.value -= 10
        if (Ninja.health === 0) {
            alert('LeeroyJenkins has won!!!')
            location.reload()
        }
        currentPlayer = 'Ninja'
        playerMove.innerHTML = 'Player 1 make your move...'
    }
}

let power = document.getElementById('powerAttack')

function powerMove() {
    if (currentPlayer === 'Ninja') {
        Samurai.powerAttack()
    } else if (currentPlayer === 'Samurai') {
        Ninja.powerAttack()
    }
}

power.addEventListener('click', powerMove)

//light attack
Player.prototype.lightAttack = function() {
    if (currentPlayer === 'Ninja') {
        Samurai.health -= 5
        playerTwoHealth.value -= 5
        if (Samurai.health === 0) {
            alert('SneakyBoi has won!!!')
            location.reload()
        }
        currentPlayer = 'Samurai'
        playerMove.innerHTML = 'Player 2 make your move...'
    } else if (currentPlayer === 'Samurai') {
        Ninja.health -= 5
        playerOneHealth.value -= 5
        if (Ninja.health === 0) {
            alert('LeeroyJenkins has won!!!')
            location.reload()
        }
        currentPlayer = 'Ninja'
        playerMove.innerHTML = 'Player 1 make your move...'
    }
}

let light = document.getElementById('lightAttack')

function lightMove() {
    if (currentPlayer === 'Ninja') {
        Samurai.lightAttack()
    } else if (currentPlayer === 'Samurai') {
        Ninja.lightAttack()
    }
}

light.addEventListener('click', lightMove)

//player healing move
Player.prototype.heal = function() {
    if (currentPlayer === 'Ninja') {
        Ninja.health += 10
        playerOneHealth.value += 10
        currentPlayer = 'Samurai'
        playerMove.innerHTML = 'Player 2 make your move...'
    } else if (currentPlayer === 'Samurai') {
        Samurai.health += 10
        playerTwoHealth.value += 10
        currentPlayer = 'Ninja'
        playerMove.innerHTML = 'Player 1 make your move...'
    }
}

let heal = document.getElementById('heal')

function healPlayer() {
    if (currentPlayer === 'Ninja') {
        Ninja.heal()
    } else if (currentPlayer === 'Samurai') {
        Samurai.heal()
    }
}

heal.addEventListener('click', healPlayer)